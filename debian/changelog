libdevel-mat-dumper-perl (0.50-1+apertis0) apertis; urgency=medium

  * Sync from debian/trixie.

 -- Apertis CI <devel@lists.apertis.org>  Sat, 08 Mar 2025 03:35:17 +0000

libdevel-mat-dumper-perl (0.50-1) unstable; urgency=medium

  * Import upstream version 0.50.

 -- gregor herrmann <gregoa@debian.org>  Sun, 27 Oct 2024 21:14:32 +0100

libdevel-mat-dumper-perl (0.49-1) unstable; urgency=medium

  * Import upstream version 0.49.
  * Declare compliance with Debian Policy 4.7.0.

 -- gregor herrmann <gregoa@debian.org>  Sun, 01 Sep 2024 17:22:33 +0200

libdevel-mat-dumper-perl (0.48-2) unstable; urgency=medium

  * Add a Breaks on libdevel-mat-perl < 0.53. (Closes: #1067341)

 -- gregor herrmann <gregoa@debian.org>  Wed, 20 Mar 2024 22:56:09 +0100

libdevel-mat-dumper-perl (0.48-1) unstable; urgency=medium

  * Import upstream version 0.48.
  * Update years of upstream and packaging copyright.

 -- gregor herrmann <gregoa@debian.org>  Sun, 17 Mar 2024 20:43:50 +0100

libdevel-mat-dumper-perl (0.47-1) unstable; urgency=medium

  * Import upstream version 0.47.
  * Update years of upstream and packaging copyright.
  * Declare compliance with Debian Policy 4.6.2.

 -- gregor herrmann <gregoa@debian.org>  Sun, 11 Jun 2023 21:14:51 +0200

libdevel-mat-dumper-perl (0.46-1+apertis0) apertis; urgency=medium

  * Sync from debian/bookworm.

 -- Apertis CI <devel@lists.apertis.org>  Thu, 06 Apr 2023 11:38:24 +0000

libdevel-mat-dumper-perl (0.46-1) unstable; urgency=medium

  * Import upstream version 0.46.
  * Install upstream docs.
  * Declare compliance with Debian Policy 4.6.1.

 -- gregor herrmann <gregoa@debian.org>  Sat, 17 Sep 2022 18:28:57 +0200

libdevel-mat-dumper-perl (0.45-1) unstable; urgency=medium

  * Import upstream version 0.45.

 -- gregor herrmann <gregoa@debian.org>  Tue, 05 Apr 2022 18:14:33 +0200

libdevel-mat-dumper-perl (0.44-1) unstable; urgency=medium

  * Import upstream version 0.44.

 -- gregor herrmann <gregoa@debian.org>  Sat, 19 Mar 2022 18:37:13 +0100

libdevel-mat-dumper-perl (0.43-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Update standards version to 4.6.0, no changes needed.

  [ gregor herrmann ]
  * Import upstream version 0.43.
  * Update years of upstream and packaging copyright.

 -- gregor herrmann <gregoa@debian.org>  Mon, 07 Mar 2022 20:06:30 +0100

libdevel-mat-dumper-perl (0.42-3apertis0) apertis; urgency=medium

  * Import from Debian bullseye.
  * Set component to development.

 -- Apertis package maintainers <packagers@lists.apertis.org>  Wed, 28 Apr 2021 17:44:15 +0200

libdevel-mat-dumper-perl (0.42-3) unstable; urgency=medium

  * Tighten runtime dependency on perl.
    This package compiles Perl version information into the binary module at
    build time, hence it needs to depend on the exact perl version used at
    build time.
    Thanks to Niko Tyni for the bug report and analysis.
    (Closes: #981408)
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.5.1.

 -- gregor herrmann <gregoa@debian.org>  Sun, 31 Jan 2021 01:08:00 +0100

libdevel-mat-dumper-perl (0.42-2) unstable; urgency=medium

  * Update 'DEB_BUILD_MAINT_OPTIONS = hardening=+bindnow' to '=+all'.
  * Bump debhelper-compat to 13.

 -- gregor herrmann <gregoa@debian.org>  Sat, 13 Jun 2020 03:17:37 +0200

libdevel-mat-dumper-perl (0.42-1) unstable; urgency=low

  * Initial release (closes: #958831)

 -- gregor herrmann <gregoa@debian.org>  Sat, 25 Apr 2020 19:58:49 +0200
